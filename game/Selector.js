
//var helpers = require('./helpers');

var Selector = module.exports = function(options){

  _.extend(this, options);

  var size = this.size/1.5;

  _.extend(this, {
    sprite: {
      img: window.hexagons.repository.sprites,
      x: 0,
      y: 35,
      size: 112
    },

    dA: { x: size, y: 0},
    dB: { x: -size, y: 0},
    sideA: { x: 0, y: 0},
    sideB: { x: 0, y: 0},
    rotationVel: 0.5,
    angleMove: 60,
    angle: 0,
    nextAngle: 0,
    dir: 1,

    dA2: { x: this.size, y: 0},
    dB2: { x: -this.size, y: 0},
    sideA2: { x: 0, y: 0},
    sideB2: { x: 0, y: 0},
  });

};

Selector.prototype.moveAim = function(dir){
  this.dir = dir;
  this.nextAngle += dir * this.angleMove;
};

Selector.prototype.update = function(dt){
  if (this.hexagons.selected){
    this.x = this.hexagons.selected.x;
    this.y = this.hexagons.selected.y;
  }

  function rotate(p, angle){
    var rad = angle * (Math.PI/180);
    return { 
      x: p.x * Math.cos(rad) - p.y * Math.sin(rad),
      y: p.x * Math.sin(rad) + p.y * Math.cos(rad)
    };
  }

  if (this.angle !== this.nextAngle) {
    this.angle += dt * this.rotationVel * this.dir;

     if (
        (this.dir > 0 && this.angle > this.nextAngle) ||
        (this.dir < 0 && this.angle < this.nextAngle)
      ){
        this.angle = this.nextAngle;
      }
  }
  
  var pA = rotate(this.dA, this.angle);
  var pB = rotate(this.dB, this.angle);

  this.sideA.x = this.x + pA.x;
  this.sideA.y = this.y + pA.y;
  
  this.sideB.x = this.x + pB.x;
  this.sideB.y = this.y + pB.y;

  var pA2 = rotate(this.dA2, this.angle);
  var pB2 = rotate(this.dB2, this.angle);

  this.sideA2.x = this.x + pA2.x;
  this.sideA2.y = this.y + pA2.y;
  
  this.sideB2.x = this.x + pB2.x;
  this.sideB2.y = this.y + pB2.y;

};

Selector.prototype.draw = function(ctx){

  //draw shoot line
  ctx.beginPath();
  ctx.moveTo(this.sideA2.x, this.sideA2.y);
  ctx.lineTo(this.sideB2.x, this.sideB2.y);
  ctx.strokeStyle = 'rgba(255,255,255,0.5)';
  ctx.lineWidth = 20;
  ctx.stroke();

  ctx.fillStyle = "rgba(255,255,255,0.8)";

  if (window.hexagons.settings.debug){
    ctx.save();

    //hexagon of size
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI, false);
    ctx.strokeStyle = 'rgba(255,255,255,0.8)';
    ctx.lineWidth = 4;
    ctx.stroke();

    ctx.restore();
  }

  //draw selector
  var sprite = this.sprite;
  var r = this.size;
  ctx.drawImage(sprite.img, sprite.x, sprite.y, sprite.size, sprite.size,
    this.x-r-3, this.y-r-3, r*2+8, r*2+7);
};
