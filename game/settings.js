
module.exports = {
    debug: false
  
  , size: 35 //px
  , columns: 15
  , rows: 15
  
  , maxTime: 3*60*1000 //3 minutes

  , images: {
    "sprites": "dist/images/sprites.png" 
  }

  , soundsUrl: "dist/sounds"
  , sounds: { 
    "x": {
      file: "blop_1"
    }
  }

};
