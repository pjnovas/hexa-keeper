
var Mouse = module.exports = function(options){
  this.container = options.container || window.document;

  this.events = {
    select: null,
    move: null,
    left: null,
    right: null
  };

  this.enabled = false;

  this.onSelect = this._onContainerSelect.bind(this);
  $(this.container).on('click', this.onSelect);

  this.onMove = this._onContainerMove.bind(this);
  $(this.container).on('mousemove', this.onMove);

  this.keyUp = this._onContainerKeyUp.bind(this);
  $(document).on('keyup', this.keyUp);
};

Mouse.prototype.enable = function(){
  this.enabled = true;
  return this;
};

Mouse.prototype.disable = function(){
  this.enabled = false;
  return this;
};

Mouse.prototype.on = function(evName, callback){
  if (!this.events[evName]){
    this.events[evName] = [];
  }

  this.events[evName].push(callback);

  return this;
};

Mouse.prototype._onContainerSelect = function(e){
  if (!this.enabled){
    return;
  }

  var offset = $(e.target).offset();
  var x = e.pageX - offset.left;
  var y = e.pageY - offset.top;

  _.each(this.events.select, function(cb){
    cb({ x: x, y: y });
  });
};

Mouse.prototype._onContainerMove = function(e){
  if (!this.enabled){
    return;
  }

  var offset = $(e.target).offset();
  var x = e.pageX - offset.left;
  var y = e.pageY - offset.top;

  _.each(this.events.move, function(cb){
    cb({ x: x, y: y });
  });
};

Mouse.prototype._onContainerKeyUp = function(e){
  if (!this.enabled){
    return;
  }

  var key = e.which || e.keyCode;
  var evName = null;
  switch(key){
    case 65:
    case 97:
      evName = "left";
      break;
    case 68:
    case 100:
      evName = "right";
      break;
  }

  if (!evName){
    return;
  }

  _.each(this.events[evName], function(cb){
    cb();
  });
};


