
var helpers = require('./helpers')
  , Hexagon = require('./Hexagon')
  , HexaParticles = require('./HexaParticles');

var Hexagons = module.exports = function(options){
  
  _.extend(this, options);

  _.extend(this, {
      hexagons: []
    , types: 3
    , selected: null
    , lastId: 0

    , hexaParticles: []
  });
};

Hexagons.prototype.on = function(eventName, callback){
  this.events[eventName] = callback;
  return this;
};

Hexagons.prototype.update = function(dt){
  var i;
  
  for(i=0; i<this.hexagons.length; i++){
    this.hexagons[i].update(dt);
  }

  for(i=0; i<this.hexaParticles.length; i++){
    var hp = this.hexaParticles[i];
    if (hp.particlesDone.length === 4){
      this.removeHexaParticles(hp.id);
      window.hexagons.counter[hp.type]++;
    }
    else {
      hp.update(dt);
    }
  }
};

Hexagons.prototype.draw = function(ctx){
  var i;

  for(i=0; i<this.hexagons.length; i++){
    this.hexagons[i].draw(ctx);
  }

  for(i=0; i<this.hexaParticles.length; i++){
    this.hexaParticles[i].draw(ctx);
  }
};

Hexagons.prototype.removeHexaParticles = function(id){
  for(var i=0; i<this.hexaParticles.length; i++){
    if(this.hexaParticles[i].id === id) {
      this.hexaParticles.splice(i, 1);
    }
  }
};

Hexagons.prototype.setSelection = function(pos){
 
  function getHexa(){
    for(var i=0; i<this.hexagons.length; i++){
      var hexa = this.hexagons[i];

      if (hexa.isSelectable && 
        helpers.isPointInBox(pos, {
          x: hexa.x-hexa.r,
          y: hexa.y-hexa.r,
          size: hexa.size
        })
      ){
        return hexa;
      }
    }
  }

  var found = getHexa.call(this);

  if (found){
    this.selected = found;
  }
};

Hexagons.prototype.resolveSequence = function(selector){

  function getHexa(pos){
    for(var i=0; i<this.hexagons.length; i++){
      var hexa = this.hexagons[i];

      if (
        helpers.isPointInBox(pos, {
          x: hexa.x-hexa.r,
          y: hexa.y-hexa.r,
          size: hexa.size
        })
      ){
        return hexa;
      }
    }
  }

  if (this.selected && !this.selected.removed){
    var type = this.selected.type;
    var sideA = getHexa.call(this, selector.sideA);
    var sideB = getHexa.call(this, selector.sideB);

    if (sideA && !sideA.removed && sideA.type === type){
      if (sideB && !sideB.removed && sideB.type === type){
        this.selected.remove();
        sideA.remove();
        sideB.remove();

        this.createHexaParticles(sideA);
        this.createHexaParticles(this.selected);
        this.createHexaParticles(sideB);
      }
    }
  }
};

Hexagons.prototype.createHexaParticles = function(hexa){
  var hexaParticles = new HexaParticles({
      id: ++this.lastId
    , x: hexa.x 
    , y: hexa.y
    , size: hexa.size
    , type: hexa.type
  });

  this.hexaParticles.push(hexaParticles);
};

Hexagons.prototype.createGrid = function(){
  var size = this.size,
    width = this.width;

  this.hexagons = [];
  this.lastId = 0;

  for(var j=0; j< this.columns; j++){
    for(var k=0; k< this.rows; k++){

      if ((j === 0 || j === this.columns-1) && 
          (k === 0 || k === this.rows-1) ){
        continue;
      }

      var x = j*size;
      var y = k*(size/1.2);

      var r = size/2;

      var hx = x+r + (k % 2 === 0 ? 0 : r);
      var hy = y+r;

      if (hx + r > width){
        continue;
      }

      var selectable = false;
      if (
          (k % 2 === 0 && j > 0 && j < this.columns-1 && k > 0 && k < this.rows-1) ||
          (k % 2 !== 0 && j > 0 && j < this.columns-2 && k > 0 && k < this.rows-1) 
        ) {
        selectable = true;
      }

      var type = _.random(0, this.types);
      this.hexagons.push(new Hexagon({
        id: this.lastId++,
        x: hx,
        y: hy,
        size: size,
        r: r,
        isSelectable: selectable,
        type: type,
        types: this.types
      }));
    }
  }

};
