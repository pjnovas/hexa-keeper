module.exports = {
  doBoxesVerticalCollide: function (a, b) {
    return (a.y < b.y + b.size && b.y <= a.y + a.size);
  },

  isPointInBox: function (point, box) {
    return (
      point.y > box.y && point.y < box.y + box.size && 
      point.x > box.x && point.x < box.x + box.size
    );
  },

  drawPolygon: function(ctx, x, y, radius, sides, startAngle, anticlockwise) {
    if (sides < 3) {
      return;
    }

    var a = (Math.PI * 2) / sides;
    a = anticlockwise ? -a : a;
    
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(startAngle);

    ctx.moveTo(radius, 0);
    
    for (var i = 1; i < sides; i++) {
      ctx.lineTo( radius * Math.cos(a*i), radius * Math.sin(a*i) );
    }
    
    ctx.closePath();
    ctx.restore();
  },

  timeFromMs: function(ms){
    function format(val){
      return (val < 10) ? "0" + val : val;
    }

    var t = moment.duration(ms),
      m = format(t.minutes()),
      s = format(t.seconds());

    return m + ":" + s;
  }
};