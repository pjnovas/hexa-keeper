
var helpers = require('./helpers');

var Hexagon = module.exports = function(options){

  _.extend(this, options);

  _.extend(this, {
      sprite: {
        img: window.hexagons.repository.sprites,
        x: this.type * this.size,
        y: 0,
        xr: 140,
        size: this.size
      }
    , selected: false
    , removed: false
    , fail: false
    , color: 'black'
    , loadAngle: 0
    , loadVel: 0.05
  });

};

Hexagon.prototype.remove = function(){
  this.loadAngle = 0;
  this.removed = true;
};

Hexagon.prototype.update = function(dt){

  if (this.removed) {
    this.loadAngle += this.loadVel * dt;
    if (this.loadAngle >= 360){
      this.type = _.random(0, this.types);
      this.removed = false;
    }
  }
  
  this.sprite.x = this.type * this.size;

  if (window.hexagons.settings.debug){
    switch (this.type){
      case 0: this.color = '#1B75BC'; break;
      case 1: this.color = '#72BF44'; break;
      case 2: this.color = '#F58220'; break;
      case 3: this.color = '#A3238E'; break;
    }
  }
};

Hexagon.prototype.draw = function(ctx){
  
  if (window.hexagons.settings.debug){

    //draw cube
    ctx.beginPath();
    ctx.rect(this.x-this.r, this.y-this.r, this.size, this.size);
    ctx.strokeStyle = 'silver';
    ctx.stroke();  

    if (this.removed){
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.r, 0, this.loadAngle * Math.PI / 180, false);
      ctx.fillStyle = 'gray';
      ctx.fill();
    }
    else {

      //draw hexagon
      ctx.beginPath();
      helpers.drawPolygon(ctx, this.x, this.y, this.r, 6, -Math.PI/2);
      ctx.strokeStyle = 'black';
      ctx.stroke();
      ctx.fillStyle = this.color;
      ctx.fill(); 
    }

    //point of center
    ctx.beginPath();
    ctx.arc(this.x, this.y, 3, 0, 2 * Math.PI, false);
    ctx.fillStyle = '#e5dc37';
    ctx.fill();
  }

  
  var sprite = this.sprite;
  ctx.save();

  if (!this.removed){
    ctx.beginPath();
    helpers.drawPolygon(ctx, this.x, this.y, this.r, 6, -Math.PI/2);
    ctx.closePath();
    
    ctx.save();
    ctx.fillStyle = window.hexagons.patterns[this.type];
    ctx.translate( this.x-this.r-sprite.x, this.y-this.r );
    ctx.fill();
    ctx.restore();
  }
  else {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.r/2, 0, this.loadAngle * Math.PI / 180, false);
    ctx.fillStyle = 'red';
    ctx.fill();
  }

  ctx.drawImage(sprite.img, sprite.xr+1, sprite.y, this.size-1, this.size,
                  this.x-this.r, this.y-this.r, this.size, this.size+1);

  ctx.restore();
};
