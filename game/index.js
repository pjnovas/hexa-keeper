
var Game = require('./Game')
  , Mouse = require('./input.mouse')
  , loader = require('./loader');

$(function(){

  //Game Globals
  window.hexagons = {
    repository: require('./repository'),
    sounds: require('./sounds'),
    settings: require('./settings'),
    gameTime: require('./gameTime')
  };

  function initGame(){
    var gameOptions = _.clone(hexagons.settings);
    gameOptions.canvas = document.getElementById('game-viewport');
    gameOptions.canvasSelector = document.getElementById('game-selector');
    gameOptions.input = new Mouse({
      container: gameOptions.canvasSelector
    });

    gameOptions.lives = 3;

    var game = new Game(gameOptions);
    game.init();

    $('.game-ctn')
      .width(game.width+150)
      .height(game.height);

    //begin - remove for prod
    //window.game = game;
    //game.start();
    //end

    var isFirst = true;

    $('.controls-modal a.play').on('click', function(){
      game.start();
      isFirst = false;
      $('#restart').show();
      $('#controls').show();
      $('.controls-modal').hide();
      $('.controls-modal a.play').hide();
    });

    $('#restart').on('click', function(){
      game.start();
    });

    $('#controls').on('click', function(){
      $('.controls-modal').toggle();
    });

    $('.controls-modal a.close').on('click', function(){
      if (isFirst){
        game.start();
        isFirst = false;
        $('#restart').show();
        $('#controls').show();
        $('.controls-modal a.play').hide();
      }

      $('.controls-modal').hide();
    });
  }

  loader
    .initResources(hexagons.settings.images, hexagons.settings.sounds)
    .on('error', function(err){
      window.console.log(err);
    })
    .on('report', function(prg){
      if (hexagons.settings.debug){
        window.console.log('LOADING > ' + prg);
      }

      $('#progressbar').css({
        width: prg + '%'
      });
    })
    .on('complete', function(){
      initGame();
    })
    .load();

});
