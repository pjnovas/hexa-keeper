var Hexagons = require('./Hexagons')
  , Hexagon = require('./Hexagon')
  , helpers = require('./helpers')
  , Selector = require('./Selector');

var Game = module.exports = function(options){

  _.extend(this, options);

  _.extend(this, {
      width: 0
    , height: 0
    , selector: null
    , boundGameRun: null

    , hexagons: null
    , hexaTypes: 3
    , tLoop: null
    , points: this.initialPoints
    , hexaPoints: []
  });
};

Game.prototype.on = function(eventName, callback){
  this.events[eventName] = callback;
};

Game.prototype.init = function(){

  this.ctx = this.canvas.getContext('2d');
  this.ctxSelector = this.canvasSelector.getContext('2d');

  this.width = this.columns * this.size;
  this.height = this.rows * (this.size/1.2) + this.size*0.2;

  this.canvas.width = this.width + 150;
  this.canvas.height = this.height;

  this.canvasSelector.width = this.width;
  this.canvasSelector.height = this.height;

  this.boundGameRun = this.gameRun.bind(this);

  var self = this;

  this.input
    .on('move', function(pos){
      self.hexagons.setSelection(pos);
    })
    .on('select', function(){
      self.hexagons.resolveSequence(self.selector);
    })
    .on('left', function(){
      self.selector.moveAim(-1);
    })
    .on('right', function(){
      self.selector.moveAim(1);
    });

  this.hexagons = new Hexagons({
      size: this.size
    , columns: this.columns
    , rows: this.rows
    , width: this.width
    , height: this.height
    , hexaTypes: this.hexaTypes
  });
};

Game.prototype.update = function(dt){
  this.hexagons.update(dt);
  this.selector.update(dt);

  this.currentTime = this.maxTime - hexagons.gameTime.time;
  if (this.currentTime < 0){
    this.currentTime = 0;
    this.stop();
  }
  
  this.formattedTime = helpers.timeFromMs(this.currentTime).toString();
};

Game.prototype.draw = function(ctx){
  ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  this.hexagons.draw(ctx, this.onTutorial);

  var total = 0;

  for(var i=0; i<this.hexaPoints.length; i++){
    var hp = this.hexaPoints[i];
    hp.draw(ctx);
    var points = window.hexagons.counter[i];
    total += points;
    ctx.font="20px Verdana";
    ctx.fillStyle = 'white';
    ctx.fillText("x " + points.toString(), hp.x + this.size/1.5, hp.y+5);
  }

  ctx.font="30px Verdana";
  ctx.fillText(total.toString(), this.width + 50, 230);

  ctx.font="30px Verdana";
  ctx.fillText(this.formattedTime, this.width + 50, 300);

  this.ctxSelector.clearRect(0, 0, this.canvasSelector.width, this.canvasSelector.height);
  this.selector.draw(this.ctxSelector);
};

Game.prototype.createSelector = function(){
  if (!this.selector){
    this.selector = new Selector({
      x: this.width/2,
      y: this.height/2,
      size: (this.size/2)*3,
      width: this.width,
      height: this.height,
      hexagons: this.hexagons
    });
  }
};

Game.prototype.createHexaPoints = function(){
  var size = this.size;
  var i;

  this.hexaPoints = [];
  for(i=0; i<4; i++){
    this.hexaPoints.push(new Hexagon({
      x: this.width + 50,
      y: this.hexaPoints.length * (size + 5)+size,
      size: size,
      r: size/2,
      isSelectable: false,
      type: this.hexaPoints.length,
      types: this.hexaTypes
    }));
  }

  window.hexagons.tos = [];
  window.hexagons.counter = [];
  window.hexagons.patterns = [];
  var img = window.hexagons.repository.sprites;
  for(i=0; i<this.hexaPoints.length; i++){
    var hp = this.hexaPoints[i];
    window.hexagons.tos.push({
      x: hp.x,
      y: hp.y
    });
    window.hexagons.counter.push(0);
    window.hexagons.patterns.push(this.ctx.createPattern(img, 'repeat'));
  }
};

Game.prototype.gameRun = function(){
  if (hexagons.gameTime.tick()) { this.loop(); }
  this.tLoop = window.requestAnimationFrame(this.boundGameRun);
};

Game.prototype.loop = function(){
  this.draw(this.ctx);
  this.update(hexagons.gameTime.frameTime);
};

Game.prototype.start = function(){
  this.createSelector();
  this.createHexaPoints();
  this.hexagons.createGrid();

  hexagons.gameTime.reset();
  this.gameRun();
  this.input.enable();
};

Game.prototype.restart = function(){
  this.stop();
  this.start();
};

Game.prototype.stop = function(){
  this.input.disable();
  window.cancelAnimationFrame(this.tLoop);
};
