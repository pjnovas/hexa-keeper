var helpers = require('./helpers');

var HexaParticles = module.exports = function(options){

  // id, x, y, size, type, maxHeight;
  _.extend(this, options);

  this.sprite = {
    img: window.hexagons.repository.sprites,
    x: this.type * 35,
    y: 0,
    xr: 140,
    yr: 0,
    size: this.size
  };

  _.extend(this, {
      size: this.size / 2
    , particles: []
    , gravity: 0.0005
    , particlesDone: []
  });

  this.buildParticles();
};

HexaParticles.prototype.buildParticles = function(){

  for(var i=0; i<4; i++){
    this.particles.push({
      vel: {
        x: _.random(-10, 10)/100,
        y: (_.random(15, 30)*-1)/100
      },
      x: this.x + ( (i === 1 || i === 4) ? this.size : 0),
      y: this.y + ( (i === 3 || i === 4) ? this.size : 0),
      r: _.random(5, 10),
      goingTo: false,
      outOfScreen: false
    });
  }

  this.toX = window.hexagons.tos[this.type].x;
  this.toY = window.hexagons.tos[this.type].y;
};

HexaParticles.prototype.update = function(dt){
  if (this.particlesDone.length < 4) {

    for (var i=0; i<this.particles.length; i++){
      var p = this.particles[i];

      if (p.outOfScreen){
        continue;
      }

      if (p.vel.y > 0 && !p.goingTo){
        
        //Animation init
        var d = { //vector direction
            x: this.toX - p.x
          , y: this.toY - p.y
        };
        var vLen = Math.sqrt(d.x*d.x + d.y*d.y); 

        p.dir = { //vector direction normalized
            x: d.x/vLen
          , y: d.y/vLen
        };

        p.coef = (p.y - this.toY) / (Math.pow(p.x - this.toX, 2));
        p.goingTo = true;
      }
      else if (!p.goingTo){
        p.x += p.vel.x * dt;
        p.y += p.vel.y * dt;
        p.vel.y += this.gravity * dt;
      }
      else if (p.goingTo){
        p.x += p.dir.x * dt * 0.9;
        p.y = p.coef * (Math.pow(p.x - this.toX, 2)) + this.toY;

        if (p.x > this.toX && !p.outOfScreen){
          p.outOfScreen = true;
          this.particlesDone.push(true);  
        }
      }
    }

  }
};

HexaParticles.prototype.draw = function(ctx){
  var sprite = this.sprite;

  for (var i=0; i<this.particles.length; i++){
    var p = this.particles[i];
    if (!p.outOfScreen) {

      ctx.beginPath();
      helpers.drawPolygon(ctx, p.x, p.y, p.r, 6, -Math.PI/2);
      ctx.closePath();
      
      ctx.save();
      ctx.fillStyle = ctx.createPattern(sprite.img, 'repeat');
      ctx.translate( p.x-p.r - sprite.x, p.y-p.r );
      ctx.fill();
      ctx.restore();

      ctx.drawImage(sprite.img, sprite.xr, sprite.yr, sprite.size, sprite.size,
                  p.x-p.r, p.y-p.r, p.r*2, p.r*2);

    }
  }
};
