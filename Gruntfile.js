'use strict';

module.exports = function (grunt) {
  var serverPort = 4000;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    banner: '/*! \n* <%= pkg.title || pkg.name %> - v<%= pkg.version %>' +
            '\n* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %> ' +
            '\n* <%= pkg.homepage ? pkg.homepage : "" %> ' +
            '\n*/ \n\n',

    paths: {
      game: "game/",
      vendor: "vendor/",
      dist: "dist/js/"
    },

    watch: {
      all: {
        files: ["<%= paths.game %>**/*"],
        tasks: ['watcher']
      }
    },

    builder: {
      all: {
        src: "<%= paths.game %>index.js",
        dest: "<%= paths.dist %>game.js"
      }
    },

    concat: {
      vendor: {
        src: [ '<%= paths.vendor %>**/*.js' ],
        dest: '<%= paths.dist %>vendor.js'
      },
      app: {
        options: {
          stripBanners: {
            line: true
          },
          banner: '<%= banner %>',
        },
        files: {
          '<%= paths.dist %>game.js': [ '<%= paths.dist %>game.js' ]
        }
      }
    },

    uglify: {
      all: {
        options: {
          stripBanners: {
            line: true
          },
          banner: '<%= banner %>',
        },
        files: {
          '<%= paths.dist %>game.js': [ '<%= paths.dist %>game.js' ]
        }
      }
    },

    jshint: {
      all: {
        files: {
          src: ["<%= paths.game %>**/*.js"]
        },
        options: {
            bitwise: true
          , curly: true
          , eqeqeq: true
          , forin: true
          , immed: true
          , latedef: true
          , newcap: true
          , noempty: true
          , nonew: true
          , quotmark: false
          , undef: true
          , unused: true
          , laxcomma: true

          , globals: {
              window: true
            , document: true
            , require: true
            , module: true
            , $: true
            , jQuery: true
            , _: true
            , hexagons: true
            , Howl: true
            , Howler: true
            , console: true
            , moment: true
          }
        }
      }
    }

  });

  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  
  require("./builder.grunt.js")(grunt);

  grunt.registerTask("build", [
    "jshint",
    "concat",
    "builder"
  ]);

  grunt.registerTask("watcher", [ "build" ] );

  grunt.registerTask("default", "build");
  grunt.registerTask("w", ["watcher", "watch"]);
  grunt.registerTask("dist", ["build", "uglify"]);

};